Woke up early after Ohio Linux Fest. Couldn't get back to sleep so I made a 
SHA1 clock. (This is after our Git training so I had SHA1 on my mind.)

At first I wrote it in PHP, as a wrapper around shell_exec("/bin/date | 
/usr/bin/sha1sum| /bin/cut -d' ' -f1") with a meta http-equiv redirect to 
itself after 1 second, but that struck me as inefficient (could overload 
the server at scale, haha) so I decided to try to write it in Javascript.

@brudgers on HN encouraged me to post a 'Show HN'.  Thanks!  :)